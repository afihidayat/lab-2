// LAB 8

// FB initiation function
window.fbAsyncInit = () => {
  FB.init({
    appId      : '512529212448342',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  // FB.AppEvents.logPageView();
  // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
  // dan jalankanlah fungsi render dibawah, dengan parameter true jika
  // status login terkoneksi (connected)
  // Hal ini dilakukan agar ketika web dibuka, dan ternyata sudah login, maka secara
  // otomatis akan ditampilkan view sudah login
  FB.getLoginStatus((response) => {
      render(response.status === 'connected');
  });

};
// Call init facebook. default dari facebook
(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));
// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Rubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = loginFlag => {

  if (loginFlag) {
    // Jika yang akan dirender adalah tampilan sudah login
    // Panggil Method getUserData yang anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#lab8').html(
        '<center>'+
        '<div class="profile">' +
          '<img class="picture img-circle" src="' + user.picture.data.url + '" alt="profpic" />' +
          '<div class="data">' +
            '<h1>' + user.name + '</h1>' +
            '<h2>' + user.about + '</h2>' +
            '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
          '</div>' +
        '</div>' +
        '<input id="postInput" type="text" class="logo post" placeholder="Ketik Status Anda" />' +
        '<button class="logo button-error pure-button postStatus" onclick="postStatus()">Post ke Facebook</button>' +
        '<button class="logo button-warning pure-button logout" onclick="facebookLogout()">Logout</button>'+
        '</center>'+
        '<div style="text-align:left">'+
        '<p style="font-size:50px; color:red;">Status</p>'+
        '</div>'+
        '<div style="text-align:right">'+
        '<p style="font-size:50px; color:orange;">Story</p>'+
        '</div>'


      );
      // Setelah merender tampilan diatas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story) {
            $('#lab8').append(
              '<center>'+
              '<div class="feed" id="post-'+ value.id +'" >' +
                '<h1>' + value.message + '</h1>' +
                '<h2>' + value.story + '</h2>' +
                '<button class="btn" onclick(' + deleteStatus(value.id) + ')></button>' +
              '</div>'+
              '</center>'
            );
          } else if (value.message) {
            $('#lab8').append(
              '<center>'+
              '<div class="feed teaser a" id="post-'+ value.id +'">' +
                '<h1>' + value.message + '</h1>' +
                '<button class="btn" onclick(' + deleteStatus(value.id) + ')></button>' +
              '</div>'+
              '</center>'
            );
          } else if (value.story) {
            $('#lab8').append(
              '<center>'+
              '<div class="feed teaser b" id="post-'+ value.id +'">' +
                '<h2>' + value.story + '</h2>' +
                '<button class="btn" onclick(' + deleteStatus(value.id) + ')></button>' +
              '</div>'+
              '</center>'
            );
          }
        });
      });
    });
  } else {
    // Tampilan ketika belum login
    $('#lab8').html('<center>'+'<button class="logo login" onclick="facebookLogin()">Login</button>'+'</center>');
  }
};

const facebookLogin = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
  // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
  // pada scope yang ada.
  FB.login((response) => {
    if(response.authResponse){
      console.log('login');
      console.log(response);
      console.log(response.authResponse);
      location.reload();
    }
  }, {scope:'public_profile,user_posts,publish_actions,email,user_about_me'});

};

const facebookLogout = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
  // ketika logout sukses.
  FB.getLoginStatus((response) => {
    if(response.status === 'connected'){
      FB.logout(() => {
        location.reload();
        console.log("logout");
      })
    }
  });
};

const getUserData = (render) => {
  // TODO: Implement Method Ini
  // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data User dari akun
  // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
  // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
  // tersebut
  FB.getLoginStatus((response) => {
    if(response.status === 'connected'){
      FB.api('me?fields=id,name,cover,picture,about,email,gender','GET', (response) => {
        render(response);
      })
    }
  })
};

const getUserFeed = (render) => {
  // TODO: Implement Method Ini
  // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
  // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
  // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
  // tersebut
  FB.getLoginStatus((response) => {
    if(response.status === 'connected'){
      FB.api('me?fields=feed', 'GET', (response) => {
        render(response.feed);
      })
    }
  })
};

const postFeed = messageText => {
  // Todo: Implement method ini,
  // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
  // Melalui API Facebook dengan message yang diterima dari parameter.
  FB.getLoginStatus((response) => {
    if(response.status === 'connected'){
      FB.api('me/feed', 'POST', {message:messageText}, (response) => {
        if(response.success){
          $('#postInput').val('');
        }
      })
    }
  })
};

const postStatus = () => {
  const message = $('#postInput').val();
  postFeed(message);
  location.reload();
};

$(".cta span").click(function(){
    $(".cta:not(.sent)").addClass("active");
    $("input").focus();
});

var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

$("input").on('input', function(){
    if(regex.test($(this).val())) {
        $("button").removeAttr("disabled"); }
    else {
        $("button").attr("disabled", "disabled"); }
});

$("form").submit(function(x){
    x.preventDefault();
    if(regex.test($("input").val())) {
        $(".cta span").text("Thank you!");
        $(".cta").removeClass("active").addClass("sent");
    }
});
const deleteStatus = (id) => {
  FB.getLoginStatus((response) => {
    if(response.status === 'connected'){
      FB.api(id, 'DELETE', (response) => {
        if(response.error){
          alert(response.error.type+": "+response.error.message)
        } else {
          $('#post-'+id).remove();
        }
      })
    }
  })
  event.preventDefault();
};
const deleteStatus = (id) => {
  swal({
    title: "Are you sure?",
    text: "Once deleted, you will not be able to see this post",
    icon: "warning",
    buttons: true,
    dangerMode: true
  })
  .then((willDelete) => {
    if (willDelete) {
      FB.api("/"+userID+"_"+id, "delete");
      swal("Your post has been deleted", {
        icon: "success",
      }).then((ok) => {
        window.location.reload();
      });
    }
  });
};
